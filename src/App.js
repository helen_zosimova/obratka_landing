import React, { useRef, useState, useEffect } from "react";

import { Parallax } from "react-spring/renderprops-addons";
import { ParallaxLayer } from "react-spring/renderprops-addons";

import useHorizontalScroll from "./Services/useHorizontalScroll";
import useWindowWidth from "./Services/useWindowWidth";
import { DISTANCE, MEDIA } from "./styles/utils/constants";

import Header from "./Components/Main/Header/Header";
import SlideOne from "./Components/Main/Slides/SlideOne";
import SlideTwo from "./Components/Main/Slides/SlideTwo";
import SlideThree from "./Components/Main/Slides/SlideThree";
import SlideFour from "./Components/Main/Slides/SlideFour";
import SlideFive from "./Components/Main/Slides/SlideFive";
import SlideSix from "./Components/Main/Slides/SlideSix";
import SlideSeven from "./Components/Main/Slides/SlideSeven";

import Road from "./Components/Generic/SlidesElems/Road";
import Truck from "./Components/Generic/SlidesElems/Truck";

import SlidesMobComb from "./Components/Main/SlidesMobComb/SlidesMobComb";

const App = () => {
  const parallax = useRef();
  const windowWidth = useWindowWidth();
  const [scroll, scrollWidth] = useHorizontalScroll(parallax);
  const [lifterCoordsX, setLifterCoordsX] = useState();
  const [truckCoordsX, setTruckCoordsX] = useState();
  const [containerCoordsX, setContainerCoordsX] = useState();
  const [elemPos, setElemPos] = useState(false);
  const [elemHide, setElemHide] = useState(false);

  const updateLifterCoords = (right, left) => {
    setLifterCoordsX({ right: right, left: left });
  };

  const updateTruckCoords = (right, left, top) => {
    setTruckCoordsX({ right: right, left: left, top: top });
  };

  const updateContainerCoords = (right, left) => {
    setContainerCoordsX({ right: right, left: left });
  };

  useEffect(() => {
    if (truckCoordsX && lifterCoordsX) {
      if (truckCoordsX.right - 180 < lifterCoordsX.left) {
        setElemHide(false);
      } else if (lifterCoordsX.right - 180 < truckCoordsX.left) {
        setElemHide(true);
      }
    }
  }, [lifterCoordsX, truckCoordsX]);

  useEffect(() => {
    if (truckCoordsX && containerCoordsX) {
      if (truckCoordsX.right - 250 < containerCoordsX.left) {
        setElemPos(false);
      } else if (containerCoordsX.right - 250 < truckCoordsX.left) {
        setElemPos(true);
      }
    }
  }, [truckCoordsX, containerCoordsX]);

  //define offset for text hidind according to windowWidth
  const defineOffsetDistance = () => {
    switch (true) {
      case windowWidth > MEDIA.XXL:
        return DISTANCE.LELFTXXL;
      case windowWidth > MEDIA.XL:
        return DISTANCE.LELFTXL;
      case windowWidth < MEDIA.XL:
        return DISTANCE.LEFTSXL;
      default:
        return DISTANCE.LEFTSXL;
    }
  };

  return (
    <div className="wrapper">
      {/* <Header /> */}
      <Road scroll={scroll} />
      <Truck
        scroll={scroll}
        updateCoords={updateTruckCoords}
        scrollWidth={scrollWidth}
        changeElemPos={elemPos}
      />
      {windowWidth < MEDIA.LG ? (
        <SlidesMobComb />
      ) : (
        <Parallax
          className="parallax"
          ref={parallax}
          pages={6.3}
          horizontal
          scrolling={true}
        >
          <ParallaxLayer offset={0} speed={0} factor={6.3} />
          <SlideOne offsetDistance={defineOffsetDistance()} />
          <SlideTwo />
          <SlideThree
            scroll={scroll}
            changeElemPos={elemPos}
            updateCoords={updateContainerCoords}
            truckTopCoords={truckCoordsX ? truckCoordsX.top : null}
            offsetDistance={defineOffsetDistance()}
          />
          <SlideFour offsetDistance={defineOffsetDistance()} />
          <SlideFive offsetDistance={defineOffsetDistance()} />
          <SlideSix
            changeElemVisib={elemHide}
            updateCoords={updateLifterCoords}
            scroll={scroll}
            offsetDistance={defineOffsetDistance()}
          />
          <SlideSeven />
        </Parallax>
      )}
    </div>
  );
};

export default App;
