import React, { useState } from "react";
import SVG from "react-inlinesvg";

import IconLogo from "../../../static/icons/header/logo-large.png";
import IconSandwich from "../../../static/icons/header/sandwich.svg";
import IconSandwichOpened from "../../../static/icons/header/sandwich--opened.svg";

import IconFb from "../../../static/icons/header/fb-link.svg";
import IconInsta from "../../../static/icons/header/insta-link.svg";
import IconTelega from "../../../static/icons/header/telega-link.svg";
import IconYt from "../../../static/icons/header/yt-link.svg";
import IconSc from "../../../static/icons/header/sc-link.svg";

import "./Menu.scss";

const Menu = () => {
  const [visible, setVisible] = useState(false);

  const toggleMenuVisibility = () => {
    setVisible(!visible);
  };

  return (
    <>
      <SVG
        className="menu__icon"
        src={visible ? IconSandwichOpened : IconSandwich}
        onClick={toggleMenuVisibility}
      />
      <nav
        className="menu"
        style={visible ? { display: "flex" } : { display: "none" }}
      >
        <ul className="menu__list">
          <li className="menu__item  menu__item--logo">
            <img className="menu__logo" src={IconLogo} />
          </li>
          <li className="menu__item ">
            <span className="menu__link">Меню</span>
          </li>
          <li className="menu__item ">
            <span className="menu__link">Меню</span>
          </li>
        </ul>
        <div className="social">
          <SVG className="social__link" src={IconFb} />
          <SVG className="social__link" src={IconInsta} />
          <SVG className="social__link" src={IconTelega} />
          <SVG className="social__link" src={IconYt} />
          <SVG className="social__link" src={IconSc} />
        </div>
      </nav>
    </>
  );
};

export default Menu;
