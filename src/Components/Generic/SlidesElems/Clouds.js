import React from "react";
import SVG from "react-inlinesvg";

import { ParallaxLayer } from "react-spring/renderprops-addons";

import IconCloud_1 from "../../../static/icons/slides/cloud_1.svg";
import IconCloud_2 from "../../../static/icons/slides/cloud_2.svg";
import IconCloud_3 from "../../../static/icons/slides/cloud_3.svg";
import IconCloud_4 from "../../../static/icons/slides/cloud_4.svg";

import "./SlidesElems.scss";

function Clouds({ offset_1, offset_2}) {
  return (
    <>
      <ParallaxLayer offset={offset_1} speed={0.4}>
        <div className="clouds">
          <SVG className="cloud cloud__one" src={IconCloud_1} />
          <SVG className="cloud cloud__two" src={IconCloud_2} />
        </div>
      </ParallaxLayer>

      <ParallaxLayer offset={offset_2} speed={0.2}>
        <div className="clouds">
          <SVG className="cloud cloud__three" src={IconCloud_3} />
          <SVG className="cloud cloud__four" src={IconCloud_4} />
        </div>
      </ParallaxLayer>
    </>
  );
}

export default Clouds;
