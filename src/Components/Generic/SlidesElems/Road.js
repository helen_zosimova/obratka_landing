import React from "react";
import SVG from "react-inlinesvg";

import Speed from "../../../static/icons/slides/speed.svg";

import "./SlidesElems.scss";

const Road = ({ scroll }) => {

  return (
    <div
      className="road"
      style={scroll > 0 ? { transform: `translate(${-scroll}px)` } : null}
    >
      <div className="road__speed">
        <SVG src={Speed} />
      </div>
      <hr className="road__hr road__hr--dashed"></hr>
    </div>
  );
};

export default Road;
