import React from "react";
import SVG from "react-inlinesvg";

import { ParallaxLayer } from "react-spring/renderprops-addons";

import IconTree from "../../../static/icons/slides/tree.svg";

import "./SlidesElems.scss";

const Trees = ({offset}) => {
  return (
    <>
      <ParallaxLayer offset={offset} speed={0}>
        <div className="tree-wrap">
          <SVG src={IconTree} />
          <SVG src={IconTree} />
          <SVG src={IconTree} />
          <SVG src={IconTree} />
        </div>
      </ParallaxLayer>
    </>
  );
};

export default Trees;
