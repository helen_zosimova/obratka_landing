import React, { useRef, useEffect } from "react";
import SVG from "react-inlinesvg";

import IconTruck from "../../../static/icons/slides/truck.svg";

import "./SlidesElems.scss";

const Truck = ({ scroll, updateCoords, changeElemPos, scrollWidth }) => {
  const truckRef = useRef();

  useEffect(() => {
    if (truckRef.current) {
      updateCoords(
        truckRef.current.getBoundingClientRect().right,
        truckRef.current.getBoundingClientRect().left,
        truckRef.current.getBoundingClientRect().top
      );
    }
  }, [scroll]);

  return (
    <div ref={truckRef} className="truck">
      <SVG src={IconTruck} />
      <div
        className="truck__container"
        style={
          changeElemPos
            ? { display: "block", transition: "1s" }
            : { display: "none", transition: "0.5s" }
        }
      />

      <div className="wheel wheel--first">
        <div
          className="wheel__shadow"
          style={scroll > 0 ? { transform: `rotate(${scroll}deg)` } : null}
        ></div>
      </div>
      <div className="wheel wheel--second">
        <div
          className="wheel__shadow"
          style={scroll > 0 ? { transform: `rotate(${scroll}deg)` } : null}
        ></div>
      </div>
      <div className="wheel wheel--third">
        <div
          className="wheel__shadow"
          style={scroll > 0 ? { transform: `rotate(${scroll}deg)` } : null}
        ></div>
      </div>
    </div>
  );
};

export default Truck;
