import React from "react";
import SVG from "react-inlinesvg";

import Menu from "../../Generic/Menu/Menu"

import IconLogo from "../../../static/icons/header/logo.svg";

import "./Header.scss";

function Header() {
  return (
    <div className="header">
      <div className="header__wrapper">
        <Menu />
        <SVG className="header__logo" src={IconLogo} />
      </div>
    </div>
  );
}

export default Header;
