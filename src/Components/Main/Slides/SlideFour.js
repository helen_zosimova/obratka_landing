import React from "react";

import { ParallaxLayer } from "react-spring/renderprops-addons";
import VisibilitySensorFunc from "../../../Services/VisibilitySensorFunc";

import useWindowWidth from "../../../Services/useWindowWidth";
import { MEDIA } from "../../../styles/utils/constants";

import Clouds from "../../Generic/SlidesElems/Clouds";
import Trees from "../../Generic/SlidesElems/Trees";

const SlideFour = ({ offsetDistance }) => {
  const windowWidth = useWindowWidth();

  return (
    <>
      <Clouds offset_1={2.37} offset_2={2.9} />
      <Trees offset={2} speed={0} />
      <ParallaxLayer offset={windowWidth < MEDIA.XXL ? 2.1 : 2.15} speed={0}>
        <div className="info">
          <VisibilitySensorFunc
            offset={20}
            className={"info__text info__text--offset-1"}
            child={
              <p>
                Перевозчик: <br></br>
                Получайте и обрабатывайте заявки и машины с помощью <br></br>
                Вашего компьютера или смартфона.
                <br></br>
                <br></br>
                Не требует тратить время на обучение персонала.
                <br></br>
                Экономия времени и сил: меньше звонков - больше эффективности.
              </p>
            }
          />

          <VisibilitySensorFunc
            offset={offsetDistance}
            className={"info__text"}
            child={
              <p>
                Просто соберите машину в гараже, назначьте на нее водителя и
                система сразу подберет для нее заявку! <br></br>
                <br></br>
                Управляйте своим автопарком, меняя прицепы и тягачи, назначая и
                снимая водителей.<br></br>
                Отслеживайте Ваши машины в реальном времени.
              </p>
            }
          />
        </div>
      </ParallaxLayer>
    </>
  );
};

export default SlideFour;
