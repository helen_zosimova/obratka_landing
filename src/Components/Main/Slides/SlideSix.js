import React, { useRef, useEffect } from "react";
import SVG from "react-inlinesvg";

import { ParallaxLayer } from "react-spring/renderprops-addons";
import VisibilitySensorFunc from "../../../Services/VisibilitySensorFunc";
import useWindowWidth from "../../../Services/useWindowWidth";
import {MEDIA } from "../../../styles/utils/constants";

import Clouds from "../../Generic/SlidesElems/Clouds";
import LifterOne from "../../../static/icons/slides/lifter_1.svg";
import LifterTwo from "../../../static/icons/slides/lifter_2.svg";
import IconTree from "../../../static/icons/slides/tree.svg";
import IconStore from "../../../static/icons/slides/store.png";

const SlideSix = ({
  offsetDistance,
  scroll,
  updateCoords,
  changeElemVisib,
}) => {
  const lifter = useRef();
  const windowWidth = useWindowWidth();

  useEffect(() => {
    if (lifter.current) {
      updateCoords(
        lifter.current.getBoundingClientRect().right,
        lifter.current.getBoundingClientRect().left
      );
    }
  }, [scroll]);

  return (
    <>
      <Clouds offset_1={3.47} offset_2={4.1} />
      <ParallaxLayer offset={3.9} speed={0}>
        <div className="info">
          <VisibilitySensorFunc
            offset={20}
            className={"info__text"}
            child={
              <p>
                Преимущества для складской логистики. <br></br>
                <br></br> Отслеживайте в реальном времени как обрабатывается
                автотранспорт. Фиксируйте точное время прибытия и освобождения.
                Контролируйте простой и время ожидания запуска. Подключение к
                WMS системам.
              </p>
            }
          />
        </div>
      </ParallaxLayer>

      <ParallaxLayer offset={4.45} speed={0}>
        <VisibilitySensorFunc
          offset={20}
          className={"info__text info__text--offset-1"}
          child={
            <p>
              Supply chain тезисы: <br></br>
              <br></br> Трекинг есть везде - может пора ему появиться и в
              контейнерных перевозках? <br></br>
              <br></br> Опоздание машин, задержка при обработке, ошибка на
              маршруте - ничего не пропадет из поля зрения.<br></br>
              Планируйте логистику исходя из точных данных, а не туманных
              обещаний!
            </p>
          }
        />
      </ParallaxLayer>

      <ParallaxLayer offset={windowWidth < MEDIA.NOTXL ? 4.9 : 4.85} speed={0}>
        <VisibilitySensorFunc
          offset={offsetDistance}
          className={"info__text"}
          child={
            <p>
              Кем бы Вы ни были на рынке перевозок, с нами Вы всегда сможете
              рассчитывать на быстроту, удобство и безопасность при заключении
              договора. <br></br>
              <br></br> Мы искренне считаем, что, соблюдая все эти факторы и
              множество других мелочей, с Вами мы можем добиться самого лучшего
              результата!
            </p>
          }
        />
      </ParallaxLayer>

      <ParallaxLayer offset={windowWidth < MEDIA.NOTXL ? 4.15 : 4.26} speed={0}>
        <VisibilitySensorFunc
          offset={0}
          className={"store"}
          transform={true}
          child={
            <div
              className="store__img"
              style={{ backgroundImage: `url(${IconStore})` }}
            ></div>
          }
        />
      </ParallaxLayer>
      <ParallaxLayer offset={windowWidth < MEDIA.XL ? 4.68 : 4.63} speed={0}>
        <span
          className={
            changeElemVisib ? "lifter-wrap lifter-wrap--two" : "lifter-wrap"
          }
          ref={lifter}
        >
          <VisibilitySensorFunc
            offset={0}
            child={
              <SVG
                style={{ transition: "0.2s" }}
                src={changeElemVisib ? LifterTwo : LifterOne}
              />
            }
          />
        </span>
      </ParallaxLayer>

      <ParallaxLayer offset={5} speed={0}>
        <div className="tree">
          <SVG src={IconTree} />
        </div>
      </ParallaxLayer>
    </>
  );
};

export default SlideSix;
