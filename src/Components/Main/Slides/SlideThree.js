import React, { useRef, useEffect, useState } from "react";
import SVG from "react-inlinesvg";

import { ParallaxLayer } from "react-spring/renderprops-addons";
import VisibilitySensorFunc from "../../../Services/VisibilitySensorFunc";

import useWindowWidth from "../../../Services/useWindowWidth";
import { MEDIA } from "../../../styles/utils/constants";

import IconConstruction from "../../../static/icons/slides/construction.svg";
import IconContainerHold from "../../../static/icons/slides/container_hold.svg";

const SlideThree = ({
  offsetDistance,
  scroll,
  updateCoords,
  truckTopCoords,
  changeElemPos,
}) => {
  const container = useRef();
  const windowWidth = useWindowWidth();
  const [transformDistance, setTransformDistance] = useState(0);

  useEffect(() => {
    if (container.current) {
      updateCoords(
        container.current.getBoundingClientRect().right,
        container.current.getBoundingClientRect().left
      );

      console.log(container.current.getBoundingClientRect());
    }
  }, [scroll]);

  useEffect(() => {
    if (container.current) {
      const distance =
        truckTopCoords -
        Math.floor(container.current.getBoundingClientRect().top + 20);
      setTransformDistance(distance);
    }
  }, [truckTopCoords]);

  return (
    <>
      <ParallaxLayer offset={1.65} speed={0.2}>
        <div className="construction">
          <div className="construction__content">
            <SVG className="construction__hook" src={IconConstruction} />
            <SVG className="construction__hold" src={IconContainerHold} />

            <div
              ref={container}
              className="construction__container"
              style={
                changeElemPos
                  ? {
                      transform: `translateY(${transformDistance}px)`,
                      opacity: 0,
                      transition: "0.4s",
                    }
                  : { transform: "translateY(0)", transition: "0.5s" }
              }
            ></div>
          </div>
        </div>
      </ParallaxLayer>

      <ParallaxLayer offset={1.4} speed={0.2}>
        <VisibilitySensorFunc
          offset={20}
          className={"info__text info__text--offset-1"}
          child={
            <p>
              Водитель: <br></br>
              <br></br>
              Вся информация по заказу в вашем смартфоне!<br></br>
              <br></br>
              Для регистрации нужно всего-лишь скачать приложение
              <br></br>и ввести свой номер телефона.
            </p>
          }
        />
      </ParallaxLayer>
      <ParallaxLayer
        offset={windowWidth < MEDIA.NOTXL ? 1.82 : 1.789}
        speed={0.2}
      >
        <VisibilitySensorFunc
          offset={offsetDistance}
          className={"info__text"}
          child={
            <p>
              Сообщайте о своем прогрессе всем контрагентам перевозки сразу!
              <br></br>
              Всего одно нажатие и грузовладелец + брокер + диспетчер +
              складской работник знают о вашем прибытии на выгрузку.
              <br></br>
              <br></br>
              Хватит переспрашивать, уточнять готовность, ждать согласования.
              Одно нажатие и вы можете ехать!
              <br></br>
              <br></br>
              Простой интерфейс и круглосуточная поддержка - мы всегда на связи.
            </p>
          }
        />
      </ParallaxLayer>
    </>
  );
};

export default SlideThree;
