import React from "react";
import SVG from "react-inlinesvg";

import { ParallaxLayer } from "react-spring/renderprops-addons";

import Clouds from "../../Generic/SlidesElems/Clouds";
import IconPort from "../../../static/icons/slides/port.svg";


const SlideTwo = () => {
  return (
    <>
      <Clouds offset_1={1.07} offset_2={1.75} />

      <ParallaxLayer offset={1.25} speed={0.1}>
        <div className="port">
          <SVG src={IconPort} />
        </div>
      </ParallaxLayer>
    </>
  );
};

export default SlideTwo;
