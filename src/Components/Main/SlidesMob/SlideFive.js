import React from "react";

import { ParallaxLayer } from "react-spring/renderprops-addons";
import VisibilitySensorFunc from "../../../Services/VisibilitySensorFunc";

import ImgTown from "../../../static/icons/slides/town.png";

const SlideFive = ({offsetDistance}) => {
  return (
    <>
      <ParallaxLayer offset={3} speed={0}>
        <div className="info">
          <VisibilitySensorFunc
            offset={20}
            className={"info__text info__text--offset-1"}
            child={
              <p>
                Заказчик: <br></br> Размещайте и управляйте Вашими заявками в
                любой точке Украины. <br></br>
                <br></br> Работаем только с собственниками автотранспорта!
                <br></br>
                <br></br> Меньше посредников, и больше эффективности во время
                доставки.
              </p>
            }
          />
          <VisibilitySensorFunc
            offset={offsetDistance}
            className={"info__text"}
            child={
              <p>
                Просто разместите заявку и система автоматически подберет для
                Вас машину.<br></br>
                <br></br> Отслеживайте в реальном времени, куда идёт Ваш груз.
              </p>
            }
          />
        </div>
      </ParallaxLayer>
      <ParallaxLayer offset={3.55} speed={0}>
        <div className="town">
          <div
            className="town__img"
            style={{ backgroundImage: `url(${ImgTown})` }}
          ></div>
        </div>
      </ParallaxLayer>
    </>
  );
}

export default SlideFive;
