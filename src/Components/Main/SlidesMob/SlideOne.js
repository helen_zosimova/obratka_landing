import React from "react";

import { ParallaxLayer } from "react-spring/renderprops-addons";
import VisibilitySensorFunc from "../../../Services/VisibilitySensorFunc";

import useWindowWidth from "../../../Services/useWindowWidth";
import { MEDIA } from "../../../styles/utils/constants";

import Trees from "../../Generic/SlidesElems/Trees";
import Clouds from "../../Generic/SlidesElems/Clouds";

const SlideOne = ({ offsetDistance }) => {
  const windowWidth = useWindowWidth();

  return (
    <>
      <Clouds offset_1={0.08} offset_2={0.55} />
      <Trees />

      <ParallaxLayer offset={0.07} speed={0.2}>
        <VisibilitySensorFunc
          offset={20}
          className={"info__title"}
          child={
            <p>
              ОБРАТКА: <br></br> ДЕЛАЕМ ПЕРЕВОЗКИ БЫСТРЫМИ И БЕЗОПАСНЫМИ
            </p>
          }
        />
      </ParallaxLayer>
      <ParallaxLayer
        offset={windowWidth < MEDIA.XXL ? 0.54 : 0.55}
        speed={-0.6}
      >
        <VisibilitySensorFunc
          offset={offsetDistance}
          className={"info__text "}
          child={
            <p>
              Меньше звонков - больше автоматизации! <br></br> Найдите груз под
              свою машину и машину под свой груз с учетом всех мелких факторов.
              <br></br> Кем бы Вы ни были: водитель, перевозчик, заказчик - мы
              разработали Обратку специально для Вас. Обратка это сервис,
              который берет на себя поддержку Вашего бизнеса.
              <br></br>
              <br></br>
              Мы проверяем всех участников и предоставляем постоянную поддержку
              в пользовании. Благодаря нашей проверке и верификации риск работы
              с подозрительными компаниями и людьми сводится к минимуму, а
              процесс поиска и перевозки становится значительно легче!
            </p>
          }
        />
      </ParallaxLayer>
    </>
  );
};

export default SlideOne;
