import React from "react";
import SVG from "react-inlinesvg";

import { ParallaxLayer } from "react-spring/renderprops-addons";
import VisibilitySensorFunc from "../../../Services/VisibilitySensorFunc";

import Trees from "../../Generic/SlidesElems/Trees";
import IconCloud_3 from "../../../static/icons/slides/cloud_3.svg";
import IconCloud_4 from "../../../static/icons/slides/cloud_4.svg";
import IconCloud_1 from "../../../static/icons/slides/cloud_1.svg";
import IconCloud_2 from "../../../static/icons/slides/cloud_2.svg";

const SlideSeven = () => {
  return (
    <>
      <ParallaxLayer offset={4.9} speed={0.1}>
        <div className="clouds">
          <SVG className="cloud cloud__one" src={IconCloud_1} />
          <SVG className="cloud cloud__two" src={IconCloud_2} />
        </div>
      </ParallaxLayer>

      <ParallaxLayer offset={5.2} speed={0}>
        <div className="clouds clouds--last">
          <SVG className="cloud cloud__one cloud__one--last" src={IconCloud_1} />
          <SVG className="cloud cloud__three" src={IconCloud_3} />
          <SVG className="cloud cloud__two" src={IconCloud_2} />
          <SVG className="cloud cloud__four" src={IconCloud_4} />
        </div>
      </ParallaxLayer>

      <Trees offset={5.2} />
      <ParallaxLayer offset={5.3} speed={0}>
        <div className="info__wrap">
          <VisibilitySensorFunc
            offset={0}
            className="info__text"
            child={
              <p>
                Сэкономьте своё драгоценное время и зарегистрируйтесь.
                <br></br>
                <br></br> Это бесплатно сейчас и всегда.
              </p>
            }
          />
          <VisibilitySensorFunc
            offset={0}
            child={
              <div className="form">
                <input className="form__input" placeholder="e-mail"></input>
                <button className="form__btn">Subscribe</button>
              </div>
            }
          />
        </div>
      </ParallaxLayer>
    </>
  );
}

export default SlideSeven;
