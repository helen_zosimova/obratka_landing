import React from "react";
import VisibilitySensor from "react-visibility-sensor";

const VisibilitySensorFunc = ({ child, offset, className, transform }) => {
  return (
    <VisibilitySensor
      horizontal={true}
      offset={{ left: offset }} // hide element when there is less than offset px to left of the viewport
    >
      {({ isVisible }) => (
        <div
          className={className}
          style={
            transform
              ? {
                  opacity: isVisible ? 1 : 0,
                  transition: isVisible ? "0.5s" : "1s",
                  transform: isVisible ? "translateY(0)" : "translateY(-600px)",
                }
              : {
                  opacity: isVisible ? 1 : 0,
                  transition: isVisible ? "0.5s" : "0.5s",
                }
          }
        >
          {child}
        </div>
      )}
    </VisibilitySensor>
  );
};

export default VisibilitySensorFunc;
