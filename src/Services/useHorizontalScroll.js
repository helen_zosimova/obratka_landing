import { useState, useEffect, useCallback } from "react";
// import { useThrottle } from "@react-hook/throttle";

let interval, delta, scrollWidth;

const useHorizontalScroll = (ref) => {
  const [scroll, setScroll] = useState(1);

  useEffect(() => {
    ref.current.container.scrollLeft = scroll;
    // console.log(ref.current.container.scrollWidth);
    if (scroll < 0) setScroll(0);
  }, [scroll]);

  // useEffect(() => {
  //   console.log(scroll);
  // }, [scroll]);

  const handleMouseWheel = (e) => {
    if (ref.current) {
      e.preventDefault();
      clearInterval(interval);
      let distance =
        Math.abs(e.wheelDelta) > 100 ? -e.wheelDelta / 4 : -e.wheelDelta / 2;

      interval = setInterval(() => {
        const absDistance = Math.abs(distance);

        if (absDistance > 1) {
          setScroll((prevState) => prevState + distance);
        } else {
          clearInterval(interval);
        }

        distance *= 0.9;
      }, 10);

      delta = -Math.max(-1, Math.min(1, e.wheelDelta || -e.detail));
      scrollWidth = ref.current.container.scrollWidth;
    }
  };

  useEffect(() => {
    if (ref.current) {
      // nonstandard: Chrome, IE, Opera, Safari
      window.addEventListener("mousewheel", handleMouseWheel, {
        passive: false,
      });
      // nonstandard: Firefox
      window.addEventListener("DOMMouseScroll", handleMouseWheel, {
        passive: false,
      });
    }
    return () => {
      window.removeEventListener("mousewheel", handleMouseWheel, {
        passive: false,
      });
      // nonstandard: Firefox
      window.removeEventListener("DOMMouseScroll", handleMouseWheel, {
        passive: false,
      });
    };
  });

  return [scroll, delta, scrollWidth];
};

export default useHorizontalScroll;
