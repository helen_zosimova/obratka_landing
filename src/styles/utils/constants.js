export const MEDIA = {
    XXL: 1700,
    NOTXL: 1500,
    XL: 1400,
    LG: 1199,
    MD: 991,
}

export const DISTANCE = {
    LELFTXXL: 350,
    LELFTXL: 300,
    LEFTSXL: 250,
}